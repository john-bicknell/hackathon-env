#!/bin/bash
npm install pm2 -g
cd ./node_src 
npm install
pm2 stop all
pm2 delete all
pm2 start server.js --watch
cd ../react_src
npm install
npm start