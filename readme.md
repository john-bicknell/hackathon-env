Hackathon Node server & React Dev Environmnent
------------------------------------

###Recomended Way
If you can install node on your machine, you'll probably find it **a lot** easier to hack about directly than mess about with docker. 
To install:

    bash local.sh 
    
When your done run:

    pm2 kill

## -- OR --

###Docker version
If you absolutely don't want node to run on your machine, this will run both a react hot loading dev enviroment and a simple node server in seperate docker containers, you can edit code directly but you'll have to restart the containers if you want npm to reflect changes in package.json. 

Start up containers, first time you run this, you might want to go get a coffee. 

    docker-compose up -d

If you change package.json node app

    docker-compose restart node 

If you change package.json react app

    docker-compose restart react

Shutdown containers nicely

    docker-compose down
