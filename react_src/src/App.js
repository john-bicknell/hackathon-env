import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to The House Hackathon</h2>
        </div>
        <p className="App-intro">
          To get started, to edit <code>react_src/src/App.js</code> and save to reload.
        </p>
        <p  className="App-intro">
          For node server app edit <code>node_src/server.js</code> and save to reload. <br />
          <a href="http://localhost:3030">http://localhost:3030</a>
        </p>
        <p><img src="https://media.giphy.com/media/dbtDDSvWErdf2/giphy.gif" alt="fire"/></p>
        <p className="App-intro">
        Happy Hacking.
        </p>
      </div>
    );
  }
}

export default App;
