var http = require('http');

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer( (request, response) =>  {
  response.writeHead(200, {"Content-Type": "application/json"});
  response.end(JSON.stringify({ message: 'Hello Hackers' }, null, 3));
});

server.listen(3030);
